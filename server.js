// Constants

var TOTAL_PARAMS = 0x22; // 34
var c1 = [];
var c2 = [];
var b1 = new Buffer(34, 'hex');
var b2 = new Buffer(34, 'hex');

var incoming = [];
var incomingLength = 0;
var incomingExpectedLength = 0;
var incomingCallback = null;
var incomingInUse = false;

var queue = [];

var SerialPort = require("serialport").SerialPort
var serialPort = new SerialPort("/dev/tty.PL2303-00001014", {
    baudrate: 19200,
    parity: 'even'
});
var deviceAddress = 0x01;

var send = function (data, callback, expectedResponseLength) {

function doWrite(){
serialPort.write(data, function () {
  serialPort.drain(function () {
    incomingInUse = true;
    incoming = [];
    incomingLength = 0;
    incomingCallback = callback;
    incomingExpectedLength = expectedResponseLength;
  })
})
}

  // Simple Safety
  if(incomingInUse) {setTimeout(doWrite(), 500)} else {doWrite();}

};

// Register event handlers
serialPort.on("open", function () {
  console.log('port open');
  serialPort.on('data', function(data) {
    incoming.push(new Buffer(data, 'hex'));
    incomingLength++;
    if(incomingLength == incomingExpectedLength){
      incomingCallback();
      incoming = [];
      incomingLength = 0;
      incomingCallback = null;
      incomingInUse = false;
    }
  });

// Startup
setChannel(0x01, [1,1,2,2,5,6,5,4,3,2,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0], c1, b1);

getChannel(c1, b1, 1);

});

setChannel = function (channelNumber, inputChannelArray, channel, buffer) {
  var checkSum = checkSumFromArray([0x28, 0x81, channelNumber].concat(inputChannelArray));
  send([0xFB, deviceAddress, 0xFB, deviceAddress, 0x00, 0x28, 0x81, channelNumber, 0x00, 0x00, 0x00].concat(inputChannelArray).concat([checkSum]), function (error, data) {
      console.log(data);
  }, 2);
};

getChannel = function (channel, buffer, channelNumber) {

  // channel = [];

  send([0xFB, deviceAddress, 0xFB, deviceAddress,
    0x00, 0x08, 0x01, channelNumber, 0x00, 0x00, 0x00, 0x00,
    TOTAL_PARAMS, theChecksum(0x2B + channelNumber)], function (error, data) {

          buffer = Buffer.concat(data)
          buffer = buffer.slice(2, 34);
          console.log(getArrayFromBuffer(buffer));
    }, 37);
};

var checkSumFromArray = function (array) {
  var sum = 0;
  array.forEach(function (item) {
    sum = sum + item;
  });
  return theChecksum(sum);
};


getArrayFromBuffer = function (buffer) {
  return JSON.stringify(buffer);
}


var getStoredParams = function (serialPort, channel) {
  serialPort.write([0xFB, deviceAddress, 0xFB, deviceAddress,
    0x00, 0x08, 0x01, channel, 0x00, 0x00, 0x00, 0x00,
    TOTAL_PARAMS, theChecksum(0x2B + channel)], function (error, result) {


  });
};

var theChecksum = function (sum) {
  return ~sum + 1 >>> 0; // 2's complement of sum
};

// 0 -> 24 == -12db -> +12db
var storeChannelGain = function (paramIndex, value) {
  serialPort.write();
};
